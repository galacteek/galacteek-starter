import pytest

from PyQt5.QtWidgets import QApplication

from galacteek_starter import starter


class TestSplash:
    @pytest.mark.parametrize('flags', [
        []
    ])
    @pytest.mark.parametrize('wpath', [
        'test.wheel'
    ])
    @pytest.mark.parametrize('message', [
        'Cloning https://gitlab.com/galacteek/rdflib-jsonld.git',
        'Collecting rdflib-jsonld@ '
        'git+https://gitlab.com/galacteek/rdflib-jsonld.git',
        'Processing /tmp/tmpgt43wxgt/galacteek-0.5.6-py3-none-any.whl',
        'Installing collected packages: aiofiles, galacteek-ld-web4',
        'Running command git clone',
        'Cloning this'
    ])
    @pytest.mark.parametrize('error', [
        'error: who cares'
    ])
    def test_splash(self, flags, wpath,
                    message,
                    error):
        app = QApplication([])  # noqa
        splash = starter.Splash(None)
        proto = starter.PIPInstallProtocol(splash)

        with pytest.raises(ValueError):
            proto.pipe_data_received(None, None)
        with pytest.raises(ValueError):
            proto.pipe_data_received(None, 'test')

        proto.pipe_data_received(None, message.encode())

        assert splash.label.text() == message

        proto.pipe_data_received(None, error.encode())
        assert len(proto.errors) > 0

        log = proto.errors_log()
        assert error in log
