from PyQt5.QtCore import QCoreApplication


def i18n_git_not_found():
    return QCoreApplication.translate(
        'MainWindow',
        'GIT was NOT FOUND on your system !'
    )
