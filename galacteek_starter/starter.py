import logging
import asyncio
import asyncio.subprocess
import async_timeout
import hashlib
import aiohttp
import sys
import os
import os.path
import venv
import tempfile
import shutil
import traceback

from yarl import URL
from pathlib import Path
from typing import Union
from omegaconf import OmegaConf
from aiohttp.web_exceptions import HTTPOk

from distutils.version import StrictVersion
from importlib_metadata import metadata as im_metadata
from importlib_metadata import version as im_version
from importlib_metadata import PackageNotFoundError

from galacteek_ci_tools.xform import xform_run_async
from galacteek_starter.i18n import i18n_git_not_found

from PyQt5.QtWidgets import QDesktopWidget
from PyQt5.QtWidgets import QErrorMessage
from PyQt5.QtWidgets import QVBoxLayout
from PyQt5.QtWidgets import QLabel
from PyQt5.QtWidgets import QDialog
from PyQt5.QtWidgets import QSizePolicy

from PyQt5.QtGui import QPixmap
from PyQt5.QtGui import QFont
from PyQt5.QtGui import QMovie
from PyQt5.QtCore import Qt
from PyQt5.QtCore import QSize


here = Path(os.path.dirname(__file__))
splashp = here.joinpath('splash.png')
cubegifp = here.joinpath('cube.gif')


class PIPInstallProtocol(asyncio.SubprocessProtocol):
    def __init__(self, splash, debug=False):
        super().__init__()

        self.splash = splash
        self.exited = asyncio.Future()
        self.xport = None
        self.errors = []
        self._debug = debug

    def process_exited(self):
        self.exited.set_result(self.xport._returncode == 0)

    def connection_made(self, transport):
        self.xport = transport

    def errors_log(self):
        return '\n'.join(self.errors)

    def pipe_data_received(self, fd, data: bytes):
        if not isinstance(data, bytes):
            raise ValueError('Invalid data')

        try:
            msg = data.decode().strip()

            typical = [
                'Cloning',
                'Collecting',
                'Processing',
                'Installing',
                'Running',
                'Successfully'
            ]

            if msg.lower().startswith('error'):
                self.errors.append(msg)

            logging.info(f'pip: {msg}')

            for tmsg in typical:
                if msg.startswith(tmsg):
                    self.splash.gMessage(msg)
                    return
        except Exception:
            logging.info(
                f'An error ocurred while reading pipe data: '
                f'{traceback.format_exc()}'
            )


class Splash(QDialog):
    def __init__(self, pixmap, parent=None):
        super().__init__(parent)

        self.vLayout = QVBoxLayout()
        self.setLayout(self.vLayout)

        self.mov = QMovie(str(cubegifp))
        self.mov.setScaledSize(QSize(256, 256))
        self.image = QLabel()
        self.image.setMovie(self.mov)
        self.image.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.image.setAlignment(Qt.AlignCenter)
        self.image.setMinimumSize(QSize(280, 280))

        self.label = QLabel()
        self.label.setFont(QFont('Courier', 20))
        self.label.setStyleSheet('color: #4a9ea1;')
        self.label.setAlignment(Qt.AlignCenter)
        self.label.setWordWrap(True)

        self.master_label = QLabel()
        self.master_label.setFont(QFont('Courier', 26))
        self.master_label.setStyleSheet('color: red;')
        self.master_label.setAlignment(Qt.AlignCenter)
        self.master_label.setWordWrap(True)

        self.toplabel = QLabel()
        self.toplabel.setFont(QFont('Courier', 22))
        self.toplabel.setStyleSheet('color: darkorange;')
        self.toplabel.setAlignment(Qt.AlignCenter)

        self.vLayout.addWidget(self.image)
        self.vLayout.addWidget(self.master_label)
        self.vLayout.addWidget(self.toplabel)
        self.vLayout.addWidget(self.label)

        self.setStyleSheet('background-color: black;')

    def showEvent(self, ev):
        self.mov.start()

    def hideEvent(self, ev):
        self.mov.stop()

    def gMessage(self, msg):
        self.label.setText(msg)

    def masterMessage(self, msg):
        self.master_label.setText(msg)

    def topMessage(self, msg):
        self.toplabel.setText(msg)

    def topMessageClear(self):
        self.toplabel.setText('')

    def centerOnScreen(self, dw):
        self.move(dw.availableGeometry().center() - self.rect().center())


async def rungalacteek(argv):
    p = await asyncio.create_subprocess_exec(
        'galacteek',
        *argv,
        stdout=asyncio.subprocess.PIPE
    )

    await p.wait()

    return p.returncode


async def getwheel(url: URL, timeout=60 * 5,
                   sha512checksum=None, splash=None,
                   csize=1024 * 256):
    h = hashlib.sha512()
    dirp = Path(tempfile.mkdtemp())
    filep = dirp.joinpath(url.name)
    breceived = 0

    try:
        with open(filep, 'wb') as file:
            async with async_timeout.timeout(timeout):
                async with aiohttp.ClientSession() as sess:
                    async with sess.get(str(url)) as resp:
                        clength = int(resp.headers.get('Content-Length', 0))

                        if resp.status != HTTPOk.status_code:
                            raise Exception(
                                f'httpFetch: {url}: '
                                f'Invalid reply code: {resp.status}'
                            )

                        async for chunk in resp.content.iter_chunked(csize):
                            breceived += len(chunk)

                            if splash and clength != 0:
                                pct = int((breceived * 100) / clength)

                                splash.gMessage(f'{pct} % downloaded')

                            file.write(chunk)
                            h.update(chunk)
    except Exception as err:
        logging.info(f'{url}: Error fetching wheel: {err}')
    else:
        if sha512checksum is None or h.hexdigest() == sha512checksum:
            return str(filep)


async def getdistinit(url: URL, cache_fp=None, timeout=40):
    try:
        async with async_timeout.timeout(timeout):
            async with aiohttp.ClientSession() as sess:
                async with sess.get(str(url)) as resp:
                    if resp.status != HTTPOk.status_code:
                        raise Exception('HTTP error')

                    data = (await resp.read()).decode()
                    if not data:
                        raise Exception('Failed to pull init')

                    xformed = await xform_run_async(OmegaConf.create(data))

                    if not xformed:
                        raise Exception(f'{url}: xform failed')

                    if cache_fp:
                        with open(cache_fp, 'wt') as fd:
                            OmegaConf.save(xformed, fd)

                        fd.close()

                    return xformed
    except Exception as err:
        logging.info(f'{url}: Error fetching dist info: {err}')
        return None


async def pip_list(pippath):
    p = await asyncio.create_subprocess_exec(
        pippath,
        *['list'],
        stdout=asyncio.subprocess.PIPE
    )
    stdout, err = await p.communicate()
    return stdout.decode().split()


async def pip_install(pip_path: Union[str, Path],
                      to_install: Union[str, URL],
                      splash,
                      flags=[],
                      loop=None):
    """
    Generic pip install coroutine

    :param str to_install: URL of the wheel to install

    :rtype: bool
    """

    loop = loop if loop else asyncio.get_event_loop()
    pargs = [str(pip_path), 'install'] + flags

    if type(to_install) in [str, URL]:
        pargs.append(str(to_install))
    else:
        return False

    f = loop.subprocess_exec(
        lambda: PIPInstallProtocol(splash),
        *pargs,
        stdout=asyncio.subprocess.PIPE
    )

    try:
        transport, proto = await f
        await proto.exited
        return proto.exited.result()
    except Exception:
        logging.info(traceback.format_exc())
        return False


async def starter_upgrade(venv_pip_path: str,
                          wheel_url: URL,
                          splash,
                          loop=None):
    # Upgrade the starter

    result = await pip_install(
        venv_pip_path,
        str(wheel_url),
        splash,
        flags=['-U']
    )

    if result:
        splash.topMessage('Upgraded starter ...')
    else:
        splash.topMessage('Failed to upgrade starter ...')

    return result


def onerrorclose():
    sys.exit(1)


async def show_error(text: str):
    dw = QDesktopWidget()
    msg = QErrorMessage()

    msg.setMinimumSize(QSize(
        (6 * dw.width()) / 8,
        (5 * dw.height()) / 8
    ))

    msg.showMessage(text)
    msg.accepted.connect(onerrorclose)
    msg.exec()


async def bootstrap(app, loop, argv: list,
                    root: Path, venvp: Path,
                    force_url: URL = None):
    """
    Upgrade to the latest available wheel(s) and start galacteek

    :param Path root: the root applications directory path
        (in .local/share/applications) path
    :param Path venvp: the venv directory path
    :param URL force_url: force the use of a specific URL
        (for the distributions list)
    """

    pp_initial = os.environ.get('PYTHONPATH', None)

    venv_pip_path = venvp.joinpath('bin').joinpath('pip')
    init_cache_path = root.joinpath('dist-latest.yaml')
    csumcachep = root.joinpath('data').joinpath('dist_status')
    venv_sp_path = venvp.joinpath('lib').joinpath(
        f'python{sys.version_info.major}.{sys.version_info.minor}'
    ).joinpath('site-packages')

    branch = os.environ.get('GALACTEEK_BRANCH', 'pimp-my-dweb')
    defaultUrl = f'https://gitlab.com/galacteek/galacteek-starter-dist-bootstrap/-/releases/continuous-{branch}/downloads/dist-install.yaml'  # noqa

    # Make sure the user venv's packages are seen first
    if isinstance(pp_initial, str):
        os.environ['PYTHONPATH'] = f'{venv_sp_path}:{pp_initial}'
    else:
        os.environ['PYTHONPATH'] = str(venv_sp_path)

    dw = QDesktopWidget()
    splash = Splash(QPixmap(str(splashp)))
    splash.setMinimumSize(QSize(
        (6 * dw.width()) / 8,
        (3 * dw.height()) / 8
    ))
    splash.setMaximumSize(QSize(
        (7 * dw.width()) / 8,
        (6 * dw.height()) / 8
    ))

    splash.centerOnScreen(dw)

    # We need git, don't we
    if not shutil.which('git') and 0:
        splash.show()
        splash.masterMessage(i18n_git_not_found())

    if not venvp.is_dir() or not venv_pip_path.is_file():
        splash.gMessage('Creating Python virtualenv')
        splash.show()

        def createvenv(vp: str):
            return venv.create(vp, with_pip=True)

        await loop.run_in_executor(None,
                                   createvenv,
                                   str(venvp))

    if isinstance(force_url, URL):
        url = str(force_url)
    else:
        url = os.environ.get(
            'GALACTEEK_DIST_URL',
            defaultUrl
        )

    splash.gMessage('Fetching dist info ..')

    init = await getdistinit(URL(url), cache_fp=init_cache_path)

    if not init:
        if init_cache_path.is_file():
            init = OmegaConf.load(str(init_cache_path))

        if init is None:
            return await show_error(
                f'Failed to download distribution map from: {url}'
            )

    # Analyze starter settings and upgrade the starter if needed
    try:
        starter_cfg = init.starter
        assert starter_cfg is not None

        starter_min_v = StrictVersion(starter_cfg.requires_version)
        starter_dists = starter_cfg.dist

        proj_meta = im_metadata('galacteek_starter')
        starter_version = StrictVersion(proj_meta['Version'])

        logging.info(
            f'Current starter version: {starter_version}. '
            f'Required version: {starter_min_v}'
        )

        if starter_min_v and starter_min_v > starter_version:
            splash.topMessage(f'Installing starter v{starter_min_v}')
            splash.show()

            proj_homeurl = starter_cfg.get(
                'git_project_url',
                proj_meta.get(
                    'Home-Page',
                    'https://gitlab.com/galacteek/galacteek-starter'
                )
            )

            logging.info(f'Proj Home URL: {proj_homeurl}')

            for sdist in starter_dists:
                splash.topMessage(
                    f'Installing starter wheel: {sdist.disturl}'
                )

                if await starter_upgrade(venv_pip_path,
                                         URL(sdist.disturl),
                                         splash):
                    splash.topMessage(
                        f'Starter wheel installed: {sdist.disturl}'
                    )
                    logging.info(f'Starter wheel installed: {sdist.disturl}')
                    logging.info(f'Starter version: {starter_min_v}')
                else:
                    splash.topMessage(
                        f'Wheel installation failed: {sdist.disturl}'
                    )
                    if 0:
                        await show_error(
                            'Error upgrading starter to the required version.'
                            'Not cool.'
                        )
        else:
            logging.info('Starter is up-to-date')
    except Exception as err:
        logging.info(f'Error looking at starter configuration: {err}')

    splash.gMessage('Installing Python wheels')

    for wheel in init.dist:
        try:
            assert wheel is not None
            wheelv = StrictVersion(wheel.version)
        except (AttributeError, ValueError, TypeError, AssertionError):
            continue

        wheel_pkgname = wheel.get('pkgname')
        wheel_sha512checksum = wheel.get('sha512_checksum_hex')
        wheel_commit_sha = wheel.get('git_commit_sha')

        if not wheel_pkgname or not wheel_sha512checksum:
            continue

        cached_csump = csumcachep.joinpath(f'iwheel.{wheel_pkgname}')
        pipiflags = list(wheel.get('pip_install_flags', []))

        try:
            cached_csum, cached_commit_sha = None, None
            version = StrictVersion(im_version(wheel_pkgname))

            if cached_csump.is_file():
                with open(str(cached_csump), 'rt') as sfd:
                    line = sfd.readline().strip()

                    # sha512 hash's size is 64 bytes, 128 chars as hex string
                    if len(line) == 128:
                        cached_csum = line

                    # git commit
                    line = sfd.readline().strip()
                    if line:
                        cached_commit_sha = line
        except (PackageNotFoundError, BaseException) as err:
            logging.info(f'Error reading version: {err}')

            version = None

        if cached_commit_sha and cached_commit_sha == wheel_commit_sha:
            logging.info(f'{wheel.pkgname}: Already got dist with '
                         f'commit: {cached_csum} installed')
            continue
        elif cached_csum and cached_csum == wheel_sha512checksum:
            logging.info(f'{wheel.pkgname}: Already got dist with '
                         f'checksum: {cached_csum} installed')
            continue
        elif (version and version >= wheelv) and 0:
            # version checking. Disabled for now, since we definitely
            # want to be able to push updates without revbumping all
            # the time
            continue

        if not splash.isVisible():
            splash.show()

        splash.gMessage(f'Fetching: {url}')

        url = URL(wheel.disturl)

        splash.topMessage(f'Fetching wheel: {wheel_pkgname}')

        for att in range(0, 3):
            wpath = await getwheel(url,
                                   sha512checksum=wheel_sha512checksum,
                                   splash=splash)
            if wpath:
                break
            else:
                splash.gMessage(f'Retrying ({att}): {url}')

                await asyncio.sleep(5)

        if not wpath:
            return await show_error(f'Failed to install wheel: {url}')

        flags = ['-U', '--require-virtualenv'] + pipiflags

        if (version and version == wheelv):
            # Different checksum but same version
            # XXX: Disabled force-reinstall: strict versioning is the right way

            logging.info(f'Wheel version: {wheelv} is '
                         'the same as the installed wheel')

        splash.topMessage(f'Installing wheel: {wheel_pkgname}')

        f = loop.subprocess_exec(
            lambda: PIPInstallProtocol(splash),
            *[str(venv_pip_path), 'install'] + flags + [wpath],
            stdout=asyncio.subprocess.PIPE
        )

        try:
            transport, proto = await f
            await proto.exited
            result = proto.exited.result()
        except Exception as err:
            os.unlink(wpath)

            logging.info(traceback.format_exc())
            logging.info(f'Error installing {wheel_pkgname}: {err}')

            return await show_error(
                f'Unknown error during wheel install: {wheel_pkgname}: {err}'
            )
        else:
            splash.topMessageClear()
            os.unlink(wpath)

            if result is False:
                return await show_error(
                    f"Error installing wheel: {wheel_pkgname}\n\n"
                    "This is the pip error log:\n\n"
                    "---\n"
                    f"{proto.errors_log()}\n"
                    "---\n"
                )
            else:
                try:
                    with open(str(cached_csump), 'wt') as sfd:
                        sfd.write(f'{wheel_sha512checksum}\n')

                        if wheel_commit_sha:
                            sfd.write(f'{wheel_commit_sha}\n')

                    sfd.close()
                except Exception as err:
                    logging.info(f'Error storing installed wheel info: {err}')

    splash.hide()
    splash.deleteLater()

    if not shutil.which('galacteek'):
        return await show_error(
            "The galacteek wheel doesn't seem to be installed."
            "Check your internet connectivity."
        )

    # Why ?
    del app

    sys.exit(await rungalacteek(argv))
