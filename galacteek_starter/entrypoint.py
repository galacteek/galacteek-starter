import sys
import logging
import asyncio
import asyncio.subprocess
import shutil
import os
import os.path

from pathlib import Path
from qasync import QEventLoop

from PyQt5.QtCore import QStandardPaths
from PyQt5.QtWidgets import QApplication

from galacteek_starter import starter

pyv = f'{sys.version_info.major}.{sys.version_info.minor}'


def venvsitepackages(venvp: Path):
    return str(venvp.joinpath('lib').joinpath(
        f'python{pyv}').joinpath('site-packages'))


def venv_configure() -> (Path, Path):
    """
    Compute the venv paths.

    Returns a tuple of Paths (root data path for the starter and
    the venv root path)
    """
    branch = os.environ.get('GALACTEEK_BRANCH', 'pimp-my-dweb')

    wlocr = QStandardPaths.writableLocation(
        QStandardPaths.ApplicationsLocation)

    if not wlocr:
        print('Cannot find a writable location', file=sys.stderr)

        sys.exit(1)

    root = Path(wlocr).joinpath('galacteek').joinpath(branch)
    csumcachep = root.joinpath('data').joinpath('dist_status')

    root.mkdir(parents=True, exist_ok=True)
    csumcachep.mkdir(parents=True, exist_ok=True)

    venvp = root.joinpath('venv')
    venv_sp_path = venvp.joinpath('lib').joinpath(
        f'python{sys.version_info.major}.{sys.version_info.minor}'
    ).joinpath('site-packages')

    venvs = os.environ.get('VIRTUAL_ENV')
    ovenv = Path(venvs) if venvs and os.path.exists(venvs) else None

    if ovenv and ovenv.is_dir():
        sitep = venvsitepackages(ovenv)

        os.environ['PYTHONPATH'] = sitep + ':' + os.environ.get('PYTHONPATH', '')  # noqa

    os.environ['GVENV_SITE_PACKAGES'] = str(venv_sp_path)
    os.environ['GVENV_PATH'] = str(venvp)
    os.environ['VIRTUAL_ENV'] = str(venvp)
    os.environ['PATH'] = str(venvp.joinpath('bin')) + ':' + os.environ['PATH']

    sys.path.insert(0, venvsitepackages(venvp))

    # Logging
    logging.basicConfig(filename=str(root.joinpath('galacteek-starter.log')),
                        level=logging.INFO)

    return root, venvp


def venv_show_info(venv_p: str) -> None:
    for envv in ['GVENV_PATH',
                 'GVENV_SITE_PACKAGES',
                 'PATH',
                 'PYTHONPATH']:
        val = os.environ.get(envv, None)
        if val:
            print(f'{envv}="{val}"')


def venv_clear(venv_p: str) -> None:
    try:
        path = Path(venv_p)

        if path.is_dir():
            print(f'Remove {path} ? [Y/n]')

            resp = sys.stdin.readline().strip()

            if resp == 'Y':
                shutil.rmtree(str(path))
    except Exception:
        raise


def start_from_venv(root: Path, venv_path: Path):

    app = QApplication([])
    loop = QEventLoop(app)
    asyncio.set_event_loop(loop)

    loop.run_until_complete(
        starter.bootstrap(app, loop, sys.argv[1:],
                          root, venv_path)
    )


def start():
    root, venv_path = venv_configure()

    if root and venv_path:
        if '--venv-info' in sys.argv:
            return venv_show_info(venv_path)
        elif '--venv-clear' in sys.argv:
            return venv_clear(venv_path)

        return start_from_venv(root, venv_path)
    else:
        sys.exit(1)
