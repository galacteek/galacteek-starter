from setuptools import setup
from setuptools import find_packages

from galacteek_starter import __version__


with open('README.md', 'r') as fh:
    long_description = fh.read()

deps_links = []


def reqs_parse(path):
    reqs = []
    deps = []

    with open(path) as f:
        lines = f.read().splitlines()
        for line in lines:
            if line.startswith('-e'):
                link = line.split().pop()
                deps.append(link)
            else:
                reqs.append(line)

    return reqs


install_reqs = reqs_parse('requirements.txt')
found_packages = find_packages(exclude=['tests', 'tests.*'])

setup(
    name='galacteek-starter',
    version=__version__,
    license='GPL3',
    author='cipres',
    author_email='BM-87dtCqLxqnpwzUyjzL8etxGK8MQQrhnxnt1@bitmessage',
    url='https://gitlab.com/galacteek/galacteek-starter',
    description='Bootstrapper for galacteek',
    long_description=long_description,
    include_package_data=True,
    packages=found_packages,
    install_requires=install_reqs,
    package_data={
        '': [
            '*.yaml',
            '*.png',
            '*.gif'
        ]
    },
    entry_points={
        'gui_scripts': [
            'galacteek-starter = galacteek_starter.entrypoint:start'
        ],
    },
    classifiers=[
        'Environment :: X11 Applications :: Qt',
        'Framework :: AsyncIO',
        'Topic :: Desktop Environment :: File Managers',
        'Topic :: Internet :: WWW/HTTP :: Browsers',
        'Development Status :: 4 - Beta',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Topic :: System :: Filesystems',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9'
    ],
    keywords=[
        'asyncio',
        'aiohttp',
        'ipfs'
    ]
)
